
var
    seyeya = angular.module('seyeya',['api','imgz','txtz'])
    .directive('fullscreenRoutine',[function(){

        var link = function(scope,elem,attrs){

            scope.launchFullscreen = function(){
                var body = document.getElementsByTagName("body")[0];
                if(body.requestFullscreen) {
                    body.requestFullscreen();
                } else if(body.mozRequestFullScreen) {
                    body.mozRequestFullScreen();
                } else if(body.webkitRequestFullscreen) {
                    body.webkitRequestFullscreen();
                } else if(body.msRequestFullscreen) {
                    body.msRequestFullscreen();
                }
            }

            var n = noty({
                            layout:'topCenter',
                            text: 'Click here to launch fullscreen',
                            theme: 'relax',
                            type: 'alert',
                            callback: {
                                onCloseClick: function() {
                                    scope.launchFullscreen()
                                }
                            }
                        });
        }

        return{
            restrict:"A",
            link:link
        }
    }])
    .directive('vidRoutine',['api',function(api){
            var
                resizeVid = function(){
                    var bh = $("body").height(),
                        bw = $("body").width();

                    $("#video").css({"height":bh+"px","width":bw+"px"});

                },
                link = function(scope,elem,attrs){

                    scope.playVid = function(url){
                        var v = document.getElementById("video");
                        v.pause();
                        v.setAttribute('src',url);
                        v.load();
                        v.play();
                    };
                    scope.playVidz = function(){
                        var vidCounter = 0;
                        scope.playVid(scope.vidz[vidCounter].url);

                        $("#video").on("ended",function(){
                            vidCounter++;

                            if(vidCounter === scope.vidz.length){
                                vidCounter = 0;
                            }

                            console.log("New counter : " + vidCounter);

                            scope.playVid(scope.vidz[vidCounter].url);
                        })
                    };


                    scope.$on('vidzLoaded',function(e,args){
                        scope.vidz = args.vidz;
                        scope.playVidz();
                    });

                    resizeVid();
                    api.loadVidz();
                }
            ;

            return{
                restrict:"A",
                link:link
            }

    }])
;