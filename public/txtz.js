/**
 * Created by Bishaka on 07/08/2016.
 */
var txtzRoutine = angular.module('txtz',[])
    .directive('txtzRoutine',['api','$interval','$timeout',function(api,$interval,$timeout){

        var link = function(scope){

            scope.TxtzSliderIsBootstrapped = false;
            scope.TxtzSliderIsShutdown = false;

            scope.shutDownTxtzSlider = function(){
                scope.TxtzSliderIsShutdown = true;
                var txtz_h = $("#txtz").height(),
                    bh = $("body").height();
                TweenMax.to("#video",.5,{height:bh});
                TweenMax.to("#imgz",.5,{height:bh});
                TweenMax.to("#txtz",.5,{bottom:"-"+txtz_h});
                scope.TxtzSliderIsBootstrapped = false;
            };

            scope.setupTxtzTimer = function(){
                scope.shutdownTxtz = true;
                var checkTime =  function(){
                    var start = moment(scope.loaded_raw_txtz[0].start),
                        end = moment(scope.loaded_raw_txtz[0].end),
                        now = moment();
                    if(now.isBetween(start,end)){
                        scope.shutdownTxtz = false;
                        if(!scope.TxtzSliderIsBootstrapped){
                            scope.bootStrapTxtzSlider();
                        }
                    }else{
                        scope.shutdownTxtz = true;
                        if(!scope.TxtzSliderIsShutdown){
                            scope.shutDownTxtzSlider();
                        }
                    }
                };
                checkTime();

                if (!angular.isDefined(scope.TxtzTimer)) {
                    scope.TxtzTimer = $interval(function(){
                        if(!scope.loadingTxtz){
                            scope.loadingTxtz = true;
                            api.loadTxtz();
                        }
                    },1000);
                }


            };

            scope.rollText = function(){
                var txtz_el_w = scope.txtz_el_w,
                    secs_per_article = scope.loaded_raw_txtz[0].spa,
                    articles = scope.txtz.length,
                    total_secs = secs_per_article * articles;

                TweenMax.fromTo("#txtz_entry_list",total_secs,
                    {right:"-"+txtz_el_w,left:""},
                    {left:"-"+txtz_el_w,
                        ease: Power0.easeNone,
                        onComplete:function(){
                            if(!scope.shutdownTxtz){
                                scope.rollText();
                            }
                        }});
            };

            scope.loadTxtz = function(){
                var txtz = scope.txtz;
                var txt_entries = [];
                for (var i = 0; i < txtz.length; i++) {
                    var txt = txtz[i],
                        title = $('<span class="title txtz_entry">'+txt.title+' &blacktriangleright;'+'</span>'),
                        desc = $('<span class="desc txtz_entry">'+txt.desc+'</span>');
                    txt_entries.push(title);
                    txt_entries.push(desc);
                }
                $("#txtz_entry_list").append(txt_entries);
                scope.txtz_el_w = $("#txtz_entry_list").width();
                $("#txtz_entry_list").css({"right":"-"+scope.txtz_el_w+"px"});
                scope.rollText();
            };

            scope.bootStrapTxtzSlider = function(){
                scope.TxtzSliderIsBootstrapped = true;
                scope.TxtzSliderIsShutdown = false;
                scope.txtz = scope.loaded_raw_txtz[0].txtz;
                var txtz_h = $("#txtz").height(),
                    txtz_w = $(window).width();
                TweenMax.to("#video",.5,{height:"-="+(txtz_h+20)+"px"});
                TweenMax.fromTo("#txtz",.5,
                    {bottom:"-"+txtz_h,height:txtz_h,width:txtz_w},
                    {width:txtz_w,height:txtz_h,left:0,bottom:0,
                        onComplete:function(){
                            scope.loadTxtz();
                        }});

            };

            scope.$on('txtzLoaded',function(e,args){
                scope.loadingTxtz = false;
                scope.loaded_raw_txtz = args.txtz;
                scope.setupTxtzTimer();
            });

            scope.resizeHeight = function(){
                var txtz_h = $("#txtz").height(),
                    txtz_w = $(window).width(),
                    bh = $(window).height();

                if(scope.shutdownTxtz){
                    TweenMax.to(["#video","#imgz"],.2,{height:bh});
                }else{
                    TweenMax.to(["#video","#imgz"],.2,{height:(bh-txtz_h)});
                }
            };

            $(window).on('resize',function(){
                scope.resizeHeight();
            });


            api.loadTxtz();
        };

        return{
            restrict:"A",
            link:link
        }
    }])
;