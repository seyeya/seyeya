/**
 * Created by Bishaka on 05/08/2016.
 */

var imgzRoutine = angular.module('imgz',[])
    .directive('imgzRoutine',['api','$timeout','$interval',function(api,$timeout,$interval){

        var link = function(scope){

            scope.ImgzSliderIsBootstrapped = false;
            scope.ImgzSliderIsShutdown = false;

            scope.shutDownImgzSlider = function(){
                scope.ImgzSliderIsShutdown = true;
                var img_w = scope.loaded_raw[0].width,
                    bw = $("body").width();
                TweenMax.to("#video",.5,{width:bw});
                TweenMax.to("#txtz",.5,{width:bw});
                TweenMax.to("#imgz",.5,{right:"-"+img_w});
                scope.ImgzSliderIsBootstrapped = false;
            };

            scope.setupImgzTimer = function(){
                scope.shutdownImgz = true;
                var checkTime =  function(){
                    var start = moment(scope.loaded_raw[0].start),
                        end = moment(scope.loaded_raw[0].end),
                        now = moment();
                    if(now.isBetween(start,end)){
                        scope.shutdownImgz = false;
                        if(!scope.ImgzSliderIsBootstrapped){
                            scope.bootStrapImgzSlider();
                        }
                    }else{
                        scope.shutdownImgz = true;
                        if(!scope.ImgzSliderIsShutdown){
                            scope.shutDownImgzSlider();
                        }
                    }
                };
                checkTime();

                if (!angular.isDefined(scope.ImgzTimer)) {
                    scope.ImgzTimer = $interval(function(){
                        if(!scope.loadingImgz){
                            scope.loadingImgz = true;
                            api.loadImgz();
                        }
                    },1000);
                }
            };

            scope.loadImg = function(){
                scope.imgCounter++;

                if(scope.imgCounter === scope.imgz.length){
                    scope.imgCounter = 0;
                }

                var img_w = scope.loaded_raw[0].width, timeout = scope.imgz[scope.imgCounter].imgDur || scope.loaded_raw[0].imgDur || 3000;

                TweenMax.to("#img_"+scope.next_img,0,{right:"-"+img_w});
                $("#img_"+scope.next_img).css({"background-image":"url("+scope.imgz[scope.imgCounter].url+")"});

                TweenMax.to(["#img_"+scope.curr_img,"#img_"+scope.next_img],.5,{
                    right:"+="+img_w,
                    onComplete:function(){
                        $timeout(function(){
                            if(!scope.shutdownImgz){
                                scope.loadImg();
                            }
                        },timeout);
                    }});

                var cache = scope.curr_img;
                scope.curr_img = scope.next_img;
                scope.next_img = cache;
            };

            scope.bootStrapImgzSlider = function(){
                scope.ImgzSliderIsBootstrapped = true;
                scope.ImgzSliderIsShutdown = false;
                scope.imgCounter = -1;
                scope.curr_img = "a";
                scope.next_img = "b";

                var img_w = scope.loaded_raw[0].width,img_l = $("#video").width(),img_h = $("#video").height();
                scope.imgz = scope.loaded_raw[0].imgz;

                TweenMax.to("#video",.5,{width:"-="+img_w+"px"});
                //TweenMax.to("#txtz",.5,{width:"-="+img_w+"px"});
                TweenMax.to("#img_"+scope.curr_img,0,{right:0});
                TweenMax.to("#img_"+scope.next_img,0,{right:"-"+img_w});
                TweenMax.fromTo("#imgz",.5,
                                {right:"-"+img_w,height:img_h},
                                {width:img_w,height:img_h,right:0,top:0,
                                    onComplete:function(){
                                        scope.loadImg();
                                    }});

            };

            scope.$on('imgzLoaded',function(e,args){
                scope.loadingImgz = false;
                scope.loaded_raw = args.imgz;
                scope.setupImgzTimer();
            });

            scope.resizeWidth = function(){
                var img_w = scope.loaded_raw[0].width,
                    bw = $(window).width();

                TweenMax.to("#txtz",.2,{width:bw});
                //TweenMax.to("#imgz",.5,{right:"-"+img_w});
                    if(scope.shutdownImgz){
                        TweenMax.to("#video",.2,{width:bw});
                    }else{
                        TweenMax.to("#video",.2,{width:(bw-img_w)});
                    }
            };

            $(window).on('resize',function(){
                scope.resizeWidth();
            });

            api.loadImgz();
        };

        return{
            "restrict":"A",
            link:link
        }
    }])
;