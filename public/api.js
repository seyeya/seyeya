/**
 * Created by Bishaka on 05/08/2016.
 */

var api = angular.module('api',[])
    .factory('api',['$rootScope',function($rootScope){
        var
            vidz_host = "https://raw.githubusercontent.com/seyeya/api/master/vidz.json",
            imgz_host = "https://raw.githubusercontent.com/seyeya/api/master/imgz.json",
            txtz_host = "https://raw.githubusercontent.com/seyeya/api/master/txtz.json",
            //vidz_host = "/vidz.json",
            //imgz_host = "/imgz.json",
            //txtz_host = "/txtz.json",
            what_host = {
                "vidz" : vidz_host,
                "imgz" : imgz_host,
                "txtz" : txtz_host
            },
            load = function(what){
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                //var percentComplete = evt.loaded / evt.total * 100;
                                //$("#profileUploadProgress").html(Math.floor(percentComplete));
                            }
                        }, false);

                        xhr.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                //var percentComplete = evt.loaded / evt.total * 100;
                                //$("#profileUploadProgress").html(Math.floor(percentComplete));
                            }
                        }, false);

                        return xhr;
                    },
                    url:what_host[what],
                    type: "GET",
                    cache: false,
                    success: function(data){

                        if(typeof data === "string"){
                            data = JSON.parse(data);
                        }

                        if(data){
                            var opts = {};
                            opts[what] = data;
                            $rootScope.$broadcast(what+'Loaded',opts);
                        }else{

                        }
                    }
                });
            },
            loadVidz = function(){
                load("vidz")
            },
            loadImgz = function(){
                load("imgz")
            },
            loadTxtz = function(){
                load("txtz")
            }
        ;

        return{
            loadVidz:loadVidz,
            loadImgz:loadImgz,
            loadTxtz:loadTxtz
        }
    }])
;
