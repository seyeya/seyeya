/**
 * Created by Bishaka on 03/08/2016.
 */
var
    http = require('http'),
    express = require('express'),
    compress = require('compression'),
    app = express(),
    bodyParser = require('body-parser'),
    port =  process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0'
;

Object.assign=require('object-assign');

app.set('port',port);
app.set('ip', ip);

app.use(bodyParser.urlencoded({extended:true,limit:'50mb'}));
app.use(bodyParser.json({limit:'50mb'}));
app.use(compress());
app.use(express.static('public'));

app.listen(port, ip);
console.log('Magic happens on port ' + app.get('port'));

//http.createServer(app).listen(app.get('port'), app.get('ip'), function(){
//    console.log('Magic happens on port ' + app.get('port'));
//});

module.exports = app ;